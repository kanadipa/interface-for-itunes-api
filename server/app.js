const express = require('express');
const server = express();
const dev = process.env.NODE_ENV !== 'production'
if (dev) { require('dotenv').config() }
const host = process.env.IP || '0.0.0.0';
const port = process.env.PORT || 8080;
const fetch = require('node-fetch');
const cors = require('cors');
const logger = require("morgan");

//server.use(express.json()) //Used to parse JSON bodies
server.use(express.urlencoded({ limit: '50mb', extended: true }))////Parse URL-encoded bodies
server.use(cors())// Enable CORS
server.use(logger('dev'));//Enables logging of HTTP requests 


server.get('/search/term/:term', async (req, res) => {
    const key = 'term'
    const value = req.params.term
    const parameterkeyvalue = key + '=' + value
    const url = `https://itunes.apple.com/search?` + parameterkeyvalue;
    const apiRes = await fetch(url).catch(err => {
        res.status(500).send(err);
    });
    const unfiltered = await apiRes.json()
    if (apiRes.code && apiRes.code === 400) return;
    if (!unfiltered['resultCount']) return;
    if (!apiRes.ok) {
        throw new Error(response.statusText);
      }

    // Filter unique objects that included the searched artist name
    const si = 'artistName'
    const data = [...new Map(unfiltered.results
        .filter(x => x[si].toLowerCase().includes(value))
        .map(item =>
            [item[si], item])).values()];
    res.send({ data });
});

server.get('/lookup/albums/:id', async (req, res) => {
        const keyParameter = 'id'
        const valueParameter = req.params.id
        const parameterkeyvalue = keyParameter + '=' + valueParameter
        const keyCallback = 'entity'
        const valueCallback = 'album'
        const callback = keyCallback + '=' + valueCallback
        const url = `https://itunes.apple.com/lookup?` + parameterkeyvalue + '&' + callback;

        const apiRes = await fetch(url).catch(err => {
            res.status(500).send(err);
        });
        const unfiltered = await apiRes.json()
        if (apiRes.code && apiRes.code === 400) return;
        if (!unfiltered['resultCount']) return;
        if (!apiRes.ok) {
            throw new Error(response.statusText);
          }

        // Filter unique objects that included the searched artist ID, 
        // and avoid returning undefined IDs
        const key = 'collectionName'
        const data = [...new Map(unfiltered.results
            .filter(x => Object.keys(x).includes(key) && Object.keys(x).includes('artistId'))
            .map(item =>
                [item[key], item])).values()];

        res.send({ data });
});
server.listen(port, host, err => {
    if (err) throw err
    console.log(`Server is running on port ${port} with ip ${host}`)
})