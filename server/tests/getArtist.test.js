test("GET search/term/:term", async () => {
    const post = await Post.create({ title: "Post 1", content: "Lorem ipsum" });
  
    await supertest(app).get("/api/posts/" + post.id)
      .expect(200)
      .then((response) => {
        expect(response.body._id).toBe(post.id);
        expect(response.body.title).toBe(post.title);
        expect(response.body.content).toBe(post.content);
      });
  });