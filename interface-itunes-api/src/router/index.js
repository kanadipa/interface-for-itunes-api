import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Artist from '../views/AlbumResults.vue'

const routes = [
  { path: '/', name:'Home', component: Home },
  { path: '/artist/:id', name: 'artist', component: Artist }
]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
export default router
git init --initial-branch=main
git remote add origin git@gitlab.com:kanadipa/interface-for-itunes-api.git
git add .
git commit -m "Initial commit"
git push -u origin main
