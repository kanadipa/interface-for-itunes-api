# Interface for Itunes API - Tech Challenge

- [Architecture](#architecture-and-frameworks-used)
- [Usage](#usage)
- [Testing](#testing)

## Architecture and Frameworks used

#### *VueJS* for FrontEnd
- Server Side Rendering (SSR)
- Simplicity
- Integration

The `interface-itunes-api` folder includes the respective UI. The server is mounted separately from the back-end for scalability purposes. The frontend server runs in port 4000, which can be configured on the `vue.config.js file`. An additional configuration to the server can be adapted for further needs.


#### *Express and NodeJS* for Middleware
- Enables middleware functions
- Simplifies development
- Routing included

The `server` folder includes the respective middle and back-end configurations. The server is mounted separately from the front-end for scalability purposes.

#### *Itunes API* for Data Source
The `server` folder includes the respective api calls. The structure from the URI was followed according documenation.

<img src="client/public/structure.png" alt="architecture" width="900"/>



## Usage
To run the 2 components, VueJS, Express Server, and ITUNES API they can be run on three different terminal paths.

### Setting up the Client
```
cd client
touch .env
```
Open the .env file and write the following variables:
- VUE_APP_NODE_DEV_URL=http://localhost:8080

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Setting up the App
```
cd server
touch .env 
```
Open the .env file and write the following variables:
- NODE_ENV=development

```
npm install
npm start
```

## Testing

For testing, the framework *Jest* would be implemented both for front-end and back-end. The configurations for the tests are done separetely. DUe to the lack of time, I haven't actually implemented. 


I look forward to meeting the team! It was a fun challenge. 


