

// Router router.js File
    const express = require('express')
    const ctrl = require('../controllers/controller')
    const router = express.Router()
    const authUrl = "https://url.to.auth.system.com/invitation";
  
    // POST Request
    router.post(authUrl, ctrl.updateEmail)
    router.post(authUrl, ctrl.updateShop)

    module.exports = router
//---------------------------------

//Controller ctrl.js file
let invitationBody = req.body;
let shopId = req.params.shopId;

//Function to update Mail
    const updateEmail = async (req, res)=>{
        await User.findOneAndUpdate({
            authId: invitationResponse.body.authId
          }, {
            authId: invitationResponse.body.authId,
            email: invitationBody.email
          }, {
            upsert: true,
            new: true
          }, (err, email) =>{
            let dummyCache = {}
            if (err) {
                return res.status(400).json({ success: false, error: err })
            }
            if (!rep) {
                return res
                    .status(404)
                    .json({ success: false, error: `Object not found` })
            }
            if (dummyCache[req.headers['x-idempotence-key']]) {
                return res.status(304).send('User already invited to this shop');
              }
              return res.status(200).json({ success: true, data: email })
            })
              .catch(err => {
                res.status(422).send(err.errors);
            });
          }
    
    //function to update shop
    const updateShop = async (req, res)=>{
        await Shop.findById({shopId}, (err,shop, createdUser)=>{
            let dummyCache = {}
            if (err || !shop) {
                return res.status(500).send(err || { message: 'No shop found' });
              }
              if (shop.invitations.indexOf(invitationResponse.body.invitationId)) {
                shop.invitations.push(invitationResponse.body.invitationId);
              }
              if (shop.users.indexOf(createdUser._id) === -1) {
                shop.users.push(createdUser);
              }
              shop.save();
              if (dummyCache[req.headers['x-idempotence-key']]) {
                return res.status(304).send('User already invited to this shop');
              }
              return res.status(200).json({ success: true, data: createdUser })
        }).catch(err => {
            res.status(422).send(err.errors);
        })}

        
